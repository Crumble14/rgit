#include "rgit.hpp"

using namespace RGit;

Repository *Section::get_repo(const std::string &name)
{
	for(auto &r : repos) {
		if(r->name == name) return r.get();
	}

	return nullptr;
}

const Repository *Section::get_repo(const std::string &name) const
{
	for(const auto& r : repos) {
		if(r->name == name) return r.get();
	}

	return nullptr;
}

std::string Section::to_html() const
{
	std::string str(R"html(<section>
					<div class="section-head">
						<p><i class="fas fa-folder"></i> )html");
	str += name;
	str += R"html(</p>
					</div>

					<div class="projects">)html";

	for(const auto &r : repos) str += r->to_html();

	str += "</div>\n</section>\n";
	return str;
}

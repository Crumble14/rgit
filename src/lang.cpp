#include "rgit.hpp"

using namespace RGit;

const char* RGit::get_lang(const Language lang)
{
	const char* langs[] = {
		"assembly",
		"c",
		"cpp",
		"makefile",
		"shell",
		"java",
		"html",
		"css",
		"javascript"
	};

	return langs[lang];
}

size_t RGit::get_total_lines()
{
	size_t lines = 0;
	lines += get_lines(ASSEMBLY);
	lines += get_lines(C);
	lines += get_lines(CPP);
	lines += get_lines(MAKEFILE);
	lines += get_lines(SHELL);
	lines += get_lines(JAVA);
	lines += get_lines(HTML);
	lines += get_lines(CSS);
	lines += get_lines(JAVASCRIPT);

	return lines;
}

size_t RGit::get_lines(const Language lang)
{
	(void) lang;
	// TODO
	return 0;
}

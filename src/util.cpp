#include "rgit.hpp"

using namespace RGit;

void RGit::iterate_dir(std::string path,
	const std::function<void(const std::string&)> handle)
{
	if(path.empty()) path += '.';
	if(path.back() != '/') path += '/';

	DIR* dir;
	dirent* entry;

	if(!(dir = opendir(path.c_str()))) {
		throw std::runtime_error("Failed to open directory!");
	}

	while((entry = readdir(dir))) {
		if(entry->d_type != DT_DIR) continue;

		const std::string name(entry->d_name);
		if(name == "." || name == "..") continue;

		handle(name);
	}

	closedir(dir);
}

void RGit::foreach_split(const std::string str, const char c,
	const std::function<void(std::string&)> handle)
{
	std::stringstream ss(str);
	std::string buffer;

	while(getline(ss, buffer, c)) {
		if(buffer.empty()) return;
		handle(buffer);
	}
}

void RGit::trim(std::string& str)
{
	while(!str.empty()) {
		if(str.back() != ' ' && str.back() != '\t') break;
		str.pop_back();
	}

	while(!str.empty()) {
		if(str.front() != ' ' && str.front() != '\t') break;
		str.erase(0, 1);
	}
}

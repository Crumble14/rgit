#include "../rgit.hpp"

using namespace RGit;

static JSON::Object add_lang(const Language lang, const size_t total)
{
	const auto lines = get_lines(lang);

	JSON::Object obj;
	obj["lang"] = JSON::Value(get_lang(lang));
	obj["lines"] = JSON::Value((int) lines);
	obj["percentage"] = JSON::Value((float) (total > 0 ?
		((float) lines) * 100.0 / total : 0));

	return obj;
}

static JSON::Array get_langs(const size_t total)
{
	JSON::Array array;
	array.push(add_lang(ASSEMBLY, total));
	array.push(add_lang(C, total));
	array.push(add_lang(CPP, total));
	array.push(add_lang(MAKEFILE, total));
	array.push(add_lang(SHELL, total));
	array.push(add_lang(JAVA, total));
	array.push(add_lang(HTML, total));
	array.push(add_lang(CSS, total));
	array.push(add_lang(JAVASCRIPT, total));

	return array;
}

void RGit::total_loc_handler(Webcore::Call& call)
{
	if(call.get_path() != "/total_loc") return;

	const auto total = get_total_lines();

	JSON::Object obj;
	obj["total"] = JSON::Value((int) total);
	obj["langs"] = get_langs(total);

	call.response_fields["Content-Type"] = "application/json";
	call.response_body = (obj.dump(true));

	call.respond();
}

#include "../rgit.hpp"

using namespace RGit;

static bool is_git(const Webcore::Call &call)
{
	const auto user_agent = call.get_request_field("HTTP_USER_AGENT");
	if(!user_agent) return false;

	return (user_agent->find("git/") == 0);
}

static void handle_git(Webcore::Call &call)
{
	const auto &path = call.get_path();
	const std::string filepath(std::string(DATA_DIR)
		+ std::string(path.cbegin() + 5, path.cend()));

	if(filepath.find("..") != std::string::npos)
	{
		call.response_status = Webcore::FORBIDDEN;
		return;
	}

	call.response_body = Webcore::read_file(filepath);
	call.respond();
}

static std::string get_parent_dir(const std::string &dir)
{
	size_t pos;
	if((pos = dir.find_last_of('/')) == std::string::npos) return "/";

	return std::string(dir.cbegin(), dir.cbegin() + pos);
}

static std::string file_to_html(const File &file,
	const std::vector<std::string> &split_path)
{
	const std::string href = (file.dir ? "/repo/" : "/file/")
		+ split_path[1] + '/' + split_path[2] + file.path;

	return (file.dir ? "<a class=\"file\" href=\"" + href
		+ "\"><i class=\"fas fa-folder\"></i> " + file.name + "</a>\n"
			: "<a class=\"file\" href=\"" + href
				+ "\"><i class=\"fas fa-file\"></i> " + file.name + "</a>\n");
}

static void handle_regular(Webcore::Call &call,
	const std::vector<std::string> &split_path,
		const Section &s, const Repository &r)
{
	const auto commits = r.get_commits().size();
	const auto branches = r.get_branches().size();

	const auto &path = call.get_path();
	std::string dir(path.cbegin() + s.name.size()
		+ r.name.size() + 7, path.cend());

	if(dir.find("..") != std::string::npos)
	{
		call.response_status = Webcore::FORBIDDEN;
		return;
	}

	if(dir.empty()) dir = "/";

	std::string files;

	if(dir != "/")
	{
		files += "<a class=\"file\" href=\"/repo/"
			+ s.name + '/' + r.name + get_parent_dir(dir)
				+ "\"><i class=\"fas fa-folder\"></i> ..</a>\n";
	}

	for(const auto &f : r.get_files(r.get_head_commit(), dir))
	{
		files += file_to_html(f, split_path);
	}

	call.response_body = Webcore::read_file("templates/repo.html");
	// TODO
	Webcore::set_tags(call.response_body, "section", s.name);
	Webcore::set_tags(call.response_body, "repository", r.name);
	Webcore::set_tags(call.response_body, "commits.count",
		std::to_string(commits));
	Webcore::set_tags(call.response_body, "branches.count",
		std::to_string(branches));
	Webcore::set_tags(call.response_body, "files", files);

	call.respond();
}

void RGit::repo_handler(Webcore::Call &call)
{
	const auto &split_path = call.get_split_path();
	const auto &section = split_path[1];
	const auto &repository = call.get_split_path()[2];

	try
	{
		const auto s = get_instance()->get_section(section);

		if(!s)
		{
			call.response_status = Webcore::NOT_FOUND;
			return;
		}

		const auto r = s->get_repo(repository);

		if(!r)
		{
			call.response_status = Webcore::NOT_FOUND;
			return;
		}

		// TODO If private and not logged on, 403

		if(is_git(call))
		{
			handle_git(call);
		}
		else
		{
			handle_regular(call, split_path, *s, *r);
		}
	}
	catch(const std::exception &)
	{
		call.response_status = Webcore::INTERNAL_SERVER_ERROR;
	}
}

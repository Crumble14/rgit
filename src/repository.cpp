#include "rgit.hpp"

using namespace RGit;

// TODO Check for error on every git call (to avoid segfaults due to NULL return)

std::vector<Commit> Repository::get_commits() const
{
	std::vector<Commit> commits;
	// TODO

	return commits;
}

std::vector<std::string> Repository::get_branches() const
{
	std::vector<std::string> branches;
	// TODO

	return branches;
}

Commit Repository::get_head_commit() const
{
	Commit commit;
	git_reference_name_to_id(&commit.oid, repo, "HEAD");

	git_commit* c = nullptr;
	git_commit_lookup(&c, repo, &commit.oid);
	if(!c) throw std::runtime_error("No commit found!");

	const auto author = git_commit_author(c);
	commit.author_name = author->name;
	commit.author_email = author->email;

	commit.message = git_commit_message(c);
	// TODO Free?

	// TODO Free `c`?
	return commit;
}

static git_tree* get_tree(git_repository* repo, git_tree* tree,
	std::string path)
{
	if(path.front() == '/') path.erase(0, 1);
	if(path.empty()) return tree;

	std::string name;
	size_t pos;

	if((pos = path.find('/')) != std::string::npos) {
		name = std::string(path.cbegin(), path.cbegin() + pos);
		path.erase(0, name.size() + 1);
	} else {
		name = path;
		path.clear();
	}

	const auto size = git_tree_entrycount(tree);

	for(size_t i = 0; i < size; ++i) {
		const auto entry = git_tree_entry_byindex(tree, i);
		const auto n = git_tree_entry_name(entry);
		if(n != name) continue;

		git_tree* subtree = nullptr;
		git_tree_lookup(&subtree, repo, git_tree_entry_id(entry));

		return get_tree(repo, subtree, path);
	}

	throw std::runtime_error("File not found!");
}

std::vector<File> Repository::get_files(const Commit& commit,
	const std::string& directory) const
{
	std::vector<File> files;

	git_commit* c = nullptr;
	git_commit_lookup(&c, repo, &commit.oid);

	git_tree* tree = nullptr;
	git_commit_tree(&tree, c);

	tree = get_tree(repo, tree, directory);

	const auto size = git_tree_entrycount(tree);

	for(size_t i = 0; i < size; ++i) {
		const auto entry = git_tree_entry_byindex(tree, i);

		File file;
		file.name = git_tree_entry_name(entry);

		if(!directory.empty() && directory.back() == '/') {
			file.path = directory + file.name;
		} else {
			file.path = directory + '/'+ file.name;
		}

		file.dir = (git_tree_entry_type(entry) == GIT_OBJ_TREE);

		files.push_back(file);
	}

	sort(files.begin(), files.end(), [](const File& f1, const File& f2)
		{
			if(f1.dir == f2.dir) {
				return (f1.name.compare(f2.name) < 0);
			} else {
				return (f1.dir);
			}
		});

	return files;
}

std::string Repository::to_html() const
{
	return "<li><a class=\"project\" href=\"/repo/" + section
		+ "/" + name + "/\">" + name + "</a></li>\n";
}

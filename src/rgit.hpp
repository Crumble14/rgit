#ifndef RGIT_HPP
# define RGIT_HPP

# include <algorithm>
# include <fstream>
# include <functional>
# include <iostream>
# include <memory>
# include <sstream>
# include <string>
# include <thread>
# include <vector>

# include <dirent.h>
# include <signal.h>
# include <sys/stat.h>

# include <web_core.hpp>
# include <json.hpp>

# include <git2.h>

# define PORT	9000

# define DATA_DIR	"/home/git/"

namespace RGit
{
	void iterate_dir(std::string path,
		const std::function<void(const std::string &)> handle);
	void foreach_split(const std::string str, const char c,
		const std::function<void(std::string &)> handle);
	void trim(std::string &str);

	std::string markdown_to_html(const std::string &mk);

	enum Language : uint8_t
	{
		ASSEMBLY = 0,
		C,
		CPP,
		MAKEFILE,
		SHELL,
		JAVA,
		HTML,
		CSS,
		JAVASCRIPT
	};

	const char *get_lang(const Language lang);

	size_t get_total_lines();
	size_t get_lines(const Language lang);

	struct File
	{
		std::string name;
		std::string path;
		bool dir;
	};

	struct Commit
	{
		git_oid oid;

		std::string author_name;
		std::string author_email;
		std::string message;
	};

	struct Repository
	{
		std::string section;
		std::string name;

		git_repository *repo;

		inline Repository(const std::string &section, const std::string &name)
			: section{section}, name{name}
		{
			if(git_repository_open_bare(&repo, get_path().c_str()) != 0) {
				throw std::runtime_error("Failed to open repository!");
			}
		}

		Repository(const Repository &repo) = delete;

		inline ~Repository()
		{
			git_repository_free(repo);
		}

		inline std::string get_path() const
		{
			return std::string(DATA_DIR) + section + '/' + name;
		}

		std::vector<Commit> get_commits() const;
		std::vector<std::string> get_branches() const;
		Commit get_head_commit() const;
		std::vector<File> get_files(const Commit &commit,
			const std::string &directory) const;

		std::string to_html() const;
	};

	struct Section
	{
		std::string name;
		bool is_private;
		size_t place;

		std::vector<std::shared_ptr<Repository>> repos;

		Repository *get_repo(const std::string& name);
		const Repository *get_repo(const std::string &name) const;
		std::string to_html() const;
	};

	class Instance
	{
		public:
		inline Instance()
			: webcore(PORT)
		{}

		Instance(const Instance &) = delete;

		void register_handlers();
		void update_sections();

		inline const std::vector<Section>& get_sections() const
		{
			return sections;
		}

		Section *get_section(const std::string &name);

		void accept_client()
		{
			webcore.accept_client();
		}

		private:
		Webcore::Instance webcore;
		std::vector<Section> sections;

	};

	// TODO
	inline Instance *&get_instance()
	{
		static Instance *instance;
		return instance;
	}

	void home_handler(Webcore::Call &call);
	void total_loc_handler(Webcore::Call &call);

	void repo_handler(Webcore::Call &call);

	void assets_handler(Webcore::Call &call);

	void not_found_handler(Webcore::Call &call);
}

#endif

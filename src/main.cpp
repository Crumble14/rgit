#include "rgit.hpp"

using namespace RGit;

int main()
{
	std::cout << "Enabling RGit...\n";

	get_instance() = new Instance();

	std::cout << "Creating data directory...\n";

	mkdir(DATA_DIR, 0755);
	errno = 0;

	std::cout << "Registering handlers...\n";

	get_instance()->register_handlers();

	std::cout << "Initializing git...\n";

	git_libgit2_init();

	std::cout << "Updating sections...\n";

	get_instance()->update_sections();

	std::cout << "Ready!\n";

	while(true) get_instance()->accept_client();

	return 0;
}

NAME = rgit

CC = g++
CFLAGS = -Wall -Wextra -Werror -Wno-unused-result -std=c++17 -g3

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")

DIRS := $(shell find $(SRC_DIR) -type d)

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))

LIBS = -I ../webcore/src -I ../jsonparser/src -lstdc++ -lm -ldl -lgit2

all: $(NAME) tags

$(NAME): $(OBJ_DIRS) $(OBJ) *.a
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ) *.a $(LIBS)

$(OBJ_DIRS): $(SRC) $(HDR)
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBS)

tags: $(SRC) $(HDR)
	ctags -R $(SRC_DIR)/* --languages=c,c++

clean:
	rm -rf $(OBJ_DIRS)

fclean: clean
	rm -f $(NAME)
	rm -f tags

re: fclean all

.PHONY: all clean fclean re

start()
{
	start-stop-daemon --start --exec /etc/rgit/rgit --pidfile /etc/rgit/pidfile --background
}

stop()
{
	start-stop-daemon --stop --pidfile /etc/rgit/pidfile
}

restart()
{
	stop
	start
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		restart
		;;
	*)
		echo "Usage: {start|stop|restart}"
		exit 3
		;;
esac

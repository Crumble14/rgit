make rgit

sudo mkdir -p /etc/rgit
sudo cp -r templates/ assets/ rgit /etc/rgit
sudo cp rgit.sh /etc/init.d/rgit
sudo chmod 0755 /etc/init.d/rgit

cd /etc/rc3.d
sudo ln -s ../init.d/rgit S95rgit
